﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportProj.Routing;

namespace TransportProj
{
    /// <summary>
    /// Created to allow testing
    /// </summary>
    public static class TransportProgram
    {
        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        public static void Tick(Car car, Passenger passenger)
        {
            // check if car is at passenger and passenger needs to get in car still
            if (!passenger.IsInCar(car) && car.XPos == passenger.StartingXPos && car.YPos == passenger.StartingYPos)
            {
                car.PickupPassenger(passenger);
                passenger.GetInCar(car);
                return;
            }

            // car calculates the path at each new tick, to account for future changes where traffic changes travel times or passenger walks to different location
            ShortestPathRouter router;
            if (passenger.IsInCar(car))
            {
                // route to passenger's destination
                router = new ShortestPathRouter(car.City, car.XPos, car.YPos, passenger.DestinationXPos, passenger.DestinationYPos);
            }
            else
            {
                // route to passenger
                router = new ShortestPathRouter(car.City, car.XPos, car.YPos, passenger.StartingXPos, passenger.StartingYPos);
            }

            List<Direction> directions = router.GetDirections();
            if (directions == null || directions.Count == 0)
            {
                // routing error, somehow report error.  Could change the signature of this method to return a boolean or error message, or throw an exception
                throw new Exception("No route found from source to destination");
            }

            switch (directions[0])
            {
                case Direction.Down:
                    car.MoveDown();
                    break;

                case Direction.Up:
                    car.MoveUp();
                    break;

                case Direction.Left:
                    car.MoveLeft();
                    break;

                case Direction.Right:
                    car.MoveRight();
                    break;
            }
        }
    }
}
