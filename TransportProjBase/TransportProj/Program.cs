﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using TransportProj.Routing;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;

            City MyCity = new City(CityLength, CityWidth);
            Car car = MyCity.AddCarToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
           
            for (int i = 0; i < 25; i++)
            {
                MyCity.AddBuildingToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
            }

            Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));

            Console.WriteLine("Car:{0},{1} Passenger Start:{2},{3} Passenger Dest:{4},{5}", car.XPos, car.YPos, passenger.StartingXPos, passenger.StartingYPos, passenger.DestinationXPos, passenger.DestinationYPos);
            while(!passenger.IsAtDestination())
            {
                TransportProgram.Tick(car, passenger);
            }

            //Console.ReadKey();
            return;
        }

       

    }
}
