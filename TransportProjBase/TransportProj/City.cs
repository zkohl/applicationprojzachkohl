﻿
using System;
using System.Collections.Generic;
using TransportProj.Routing;
namespace TransportProj
{
    public class City
    {
        public static int YMax { get; private set; }
        public static int XMax { get; private set; }
        public HashSet<Coordinate> Buildings { get; private set; }

        public City(int xMax, int yMax)
        {
            XMax = xMax;
            YMax = yMax;
            Buildings = new HashSet<Coordinate>();
        }

        public Car AddCarToCity(int xPos, int yPos)
        {
            Sedan car = new Sedan(xPos, yPos, this, null);

            return car;
        }

        public void AddBuildingToCity(int xPos, int yPos)
        {
            Console.WriteLine("Adding building to city: {0},{1}", xPos, yPos);
            Buildings.Add(new Coordinate(xPos, yPos));
        }

        public Passenger AddPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            Passenger passenger = new Passenger(startXPos, startYPos, destXPos, destYPos, this);

            return passenger;
        }

        public bool HasObstacle(int xPos, int yPos)
        {
            return Buildings.Contains(new Coordinate(xPos, yPos));
        }
    }
}
