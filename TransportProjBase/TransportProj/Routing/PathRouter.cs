﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Routing
{
    public abstract class PathRouter
    {
        public int SourceX { get; private set; }
        public int DestX { get; private set; }
        public int SourceY { get; private set; }
        public int DestY { get; private set; }
        public City City { get; private set; }

        public PathRouter(City city, int sourceX, int sourceY, int destX, int destY)
        {
            this.City = city;
            this.SourceX = sourceX;
            this.SourceY = sourceY;
            this.DestX = destX;
            this.DestY = destY;
        }

        /// <summary>
        /// Returns the list of directions, null if no path found between source and dest
        /// Subclasses can determine routing logic, for example: either shortest path, no highways, less traffic, etc
        /// </summary>
        /// <returns></returns>
        public abstract List<Direction> GetDirections();
    }
}
