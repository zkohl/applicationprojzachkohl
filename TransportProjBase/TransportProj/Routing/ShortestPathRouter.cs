﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportProj.Routing;

namespace TransportProj
{
    public class ShortestPathRouter : PathRouter
    {
        /// <summary>
        /// Helper class to aid in backwards construction of the shortest path using BFS
        /// </summary>
        public class PathNode : Coordinate
        {
            public Direction DirectionTaken { get; private set; }
            public PathNode Parent { get; private set; }
            public PathNode(PathNode parent, Direction directionTaken, int x, int y)
                :base(x, y)
            {
                this.Parent = parent;
                this.DirectionTaken = directionTaken;
            }
        }

        public ShortestPathRouter(City city, int sourceX, int sourceY, int destX, int destY)
            : base(city, sourceX, sourceY, destX, destY)
        {

        }

        private bool CanVisit(int x, int y, HashSet<Coordinate> visited)
        {
            if (visited.Contains(new Coordinate(x, y)))
                return false;

            if (City.HasObstacle(x, y))
                return false;

            return x >= 0 && x < City.XMax && y >= 0 && y < City.YMax;
        }
            
        /// <summary>
        /// Use Breadth first search to find the shortest path
        /// </summary>
        /// <returns></returns>
        public override List<Direction> GetDirections()
        {
            List<Direction> directions = new List<Direction>();

            // keep queue of nodes to visit, enqueue the starting point
            Queue<PathNode> bfsQueue = new Queue<PathNode>();
            bfsQueue.Enqueue(new PathNode(null, Direction.None, SourceX, SourceY));

            // used to backwards construct the shortest path if we find one
            PathNode shortestPathEndNode = null;

            // keep track of coordinates we've visited
            HashSet<Coordinate> visited = new HashSet<Coordinate>();

            while (bfsQueue.Count >= 1)
            {
                PathNode node = bfsQueue.Dequeue();
                if (node.X == DestX && node.Y == DestY)
                {
                    // found shortest path to destination
                    shortestPathEndNode = node;
                    break;
                }

                visited.Add(new Coordinate(node.X, node.Y));

                // check up neighbor
                if (CanVisit(node.X, node.Y + 1, visited)) {
                    bfsQueue.Enqueue(new PathNode(node, Direction.Up, node.X, node.Y + 1));
                }

                // check down neighbor
                if (CanVisit(node.X, node.Y - 1, visited))
                {
                    bfsQueue.Enqueue(new PathNode(node, Direction.Down, node.X, node.Y - 1));
                }

                // check left neighbor
                if (CanVisit(node.X - 1, node.Y, visited))
                {
                    bfsQueue.Enqueue(new PathNode(node, Direction.Left, node.X - 1, node.Y));
                }

                // check right neighbor
                if (CanVisit(node.X + 1, node.Y, visited))
                {
                    bfsQueue.Enqueue(new PathNode(node, Direction.Right, node.X + 1, node.Y));
                }
            }

            // make sure we found a path
            if (shortestPathEndNode == null)
                return null;

            // work backwords to find the path taken to the destination
            Stack<Direction> path = new Stack<Direction>();
            PathNode n = shortestPathEndNode;
            while (n != null && n.Parent != null)
            {
                path.Push(n.DirectionTaken);
                n = n.Parent;
            }

            return path.ToList();
        }
    }
}
