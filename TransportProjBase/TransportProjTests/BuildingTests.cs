﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransportProj;

namespace TransportProjTests
{
    [TestClass]
    public class BuildingTests
    {
        [TestMethod]
        public void TestBuildings()
        {
            City MyCity = new City(10, 10);
            Car car = MyCity.AddCarToCity(5, 2);

            MyCity.AddBuildingToCity(5, 3);
            MyCity.AddBuildingToCity(4, 8);
            MyCity.AddBuildingToCity(6, 8);
            MyCity.AddBuildingToCity(5, 7);
            Passenger passenger = MyCity.AddPassengerToCity(5, 4, 5, 8);

            Console.WriteLine("Car:{0},{1} Passenger Start:{2},{3} Passenger Dest:{4},{5}", car.XPos, car.YPos, passenger.StartingXPos, passenger.StartingYPos, passenger.DestinationXPos, passenger.DestinationYPos);
            while (!passenger.IsAtDestination())
            {
                TransportProgram.Tick(car, passenger);
            }
        }
    }
}
